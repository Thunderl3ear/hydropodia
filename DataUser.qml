import QtQuick

ListModel{
    ListElement{ripe: true; text: "Avocado"; icon: "qrc:/icons/icons8-avocado-128.png"}
    ListElement{ripe: true; text: "Flax"; icon: "qrc:/icons/icons8-flax-seeds-100.png"}
    ListElement{ripe: false; text: "Coffee"; icon: "qrc:/icons/icons8-bean-128.png"}
    ListElement{ripe: true; text: "Strawberry"; icon: "qrc:/icons/icons8-strawberry-100.png"}
    ListElement{ripe: false; text: "Sesame"; icon: "qrc:/icons/icons8-sesame-100.png"}
    ListElement{ripe: true; text: "Plum"; icon: "qrc:/icons/icons8-plum-100.png"}
    ListElement{ripe: false; text: "Coconut"; icon: "qrc:/icons/icons8-coconut-96.png"}
    ListElement{ripe: false; text: "Mushrooms"; icon: "qrc:/icons/icons8-mushroom-160.png"}
    ListElement{ripe: true; text: "Kiwi"; icon: "qrc:/icons/icons8-kiwi-100.png"}
    ListElement{ripe: false; text: "Cherry"; icon: "qrc:/icons/icons8-berry-128.png"}
}
