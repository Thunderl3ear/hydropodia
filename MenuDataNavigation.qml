import QtQuick

ListModel{
    ListElement{pageID: 1; text: "Menu"; icon: "qrc:/icons/icons8-menu-90.png"}
    ListElement{pageID: 2; text: "Status"; icon: "qrc:/icons/icons8-garden-170.png"}
    ListElement{pageID: 3; text: "Planting"; icon: "qrc:/icons/icons8-planting-64.png"}
    ListElement{pageID: 4; text: "Environment"; icon: "qrc:/icons/icons8-sun-100.png"}
    ListElement{pageID: 0; text: "Shop"; icon: "qrc:/icons/icons8-bag-64.png"}
    ListElement{pageID: 5; text: "Settings"; icon: "qrc:/icons/icons8-settings-100.png"}
}
