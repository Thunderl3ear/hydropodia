import QtQuick

ListModel{
    ListElement{text: "Avocado"; icon: "qrc:/icons/icons8-avocado-128.png"}
    ListElement{text: "Banana"; icon: "qrc:/icons/icons8-banana-60.png"}
    ListElement{text: "Orange"; icon: "qrc:/icons/icons8-citrus-100.png"}
    ListElement{text: "Kiwi"; icon: "qrc:/icons/icons8-kiwi-100.png"}
    ListElement{text: "Watermelon"; icon: "qrc:/icons/icons8-watermelon-100.png"}
    ListElement{text: "Plum"; icon: "qrc:/icons/icons8-plum-100.png"}
}
