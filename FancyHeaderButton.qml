import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

RoundButton{
    id: toolButton
    Layout.fillHeight: true
    Layout.fillWidth: true
    onClicked: view.currentIndex = model.pageID
    background: Rectangle{
        height: parent.height
        color: Material.primary
        IconImage{
            color: (view.currentIndex===model.pageID) ? Material.foreground : parent.parent.hovered ? Material.accent : Material.background
            height: parent.height
            width: parent.height
            source: model.icon
        }
    }
}
