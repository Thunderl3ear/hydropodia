import QtQuick

ListModel{
    ListElement{text: "Sesame"; icon: "qrc:/icons/icons8-sesame-100.png"}
    ListElement{text: "Beans"; icon: "qrc:/icons/icons8-white-beans-100.png"}
    ListElement{text: "Lentils"; icon: "qrc:/icons/icons8-lentil-100.png"}
    ListElement{text: "Flax"; icon: "qrc:/icons/icons8-flax-seeds-100.png"}
    ListElement{text: "Mushrooms"; icon: "qrc:/icons/icons8-mushroom-160.png"}
}
