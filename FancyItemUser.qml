import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

RoundButton{
    id: controls
    font.pointSize: (parent.width/12 < 1) ? 1 : parent.width/12;
    implicitWidth: parent.width/3
    implicitHeight: parent.width/3
    background: Rectangle{
        height: parent.height
        radius: parent.width/20
        color: Material.background
        border.color: (model.ripe===true) ? Material.foreground : Material.primary
        border.width: parent.width/50
        IconImage{
            color: (model.ripe===true) ? Material.foreground : Material.primary
            x: parent.width-parent.height+parent.border.width
            y: parent.border.width
            height: parent.height-2*parent.border.width
            width: parent.height-2*parent.border.width
            source: model.icon
        }
        ToolTip{
            y: parent.height
            parent: controls.handle
            visible: controls.hovered
            text: model.text+((model.ripe===true) ? " is ripe" : " is not ripe")
        }
    }
}
