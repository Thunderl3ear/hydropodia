import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Item{
    id: mySettings
    height: parent.height
    width: parent.width
    anchors.margins: 0

    GridLayout{
        width: parent.width
        columns: 2
        ScalingText{text:"Theme"}
        ScalingComboBox {
            // When an item is selected, update the backend.
            onActivated: mainWindow.Material.theme = currentValue
            model: [
                { value: Material.Dark, text: "Dark"},
                { value: Material.Light, text: "Light" },
            ]
        }
        ScalingText{text:"Primary Color"}
        ScalingComboBox {
            onActivated: mainWindow.Material.primary = currentValue
            model: [
                {value: Material.BlueGrey, text: "BlueGrey"},
                {value: Material.Teal, text: "Teal"},
                {value: Material.Cyan, text: "Cyan"},
                {value: Material.Green, text: "Green"},
                {value: Material.Indigo, text: "Indigo"},
                ]
        }
        ScalingText{text:"Secondary Color"}
        ScalingComboBox {
            onActivated: mainWindow.Material.accent = currentValue
            model: [
                {value: Material.Teal, text: "Teal"},
                {value: Material.BlueGrey, text: "BlueGrey"},
                {value: Material.Cyan, text: "Cyan"},
                {value: Material.Green, text: "Green"},
                {value: Material.Indigo, text: "Indigo"},
                ]
        }
        ScalingText{text:"Text Color"}
        ScalingComboBox{
            onActivated: mainWindow.Material.foreground = currentValue
            model: [
                {value: Material.Teal, text: "Teal"},
                {value: Material.BlueGrey, text: "BlueGrey"},
                {value: Material.Cyan, text: "Cyan"},
                {value: Material.Green, text: "Green"},
                {value: Material.Indigo, text: "Indigo"},
                ]
        }
    }
}
