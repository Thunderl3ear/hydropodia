import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Item{
    height: parent.height
    width: parent.width
    anchors.margins: 0
    anchors.fill: parent
    FancyLabel{text: "Your Plants"}
    ScrollableListUser{}
}
