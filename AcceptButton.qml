import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

RoundButton{
    id: controls
    property string myText: ""
    font.pointSize: (parent.width/16 < 1) ? 1 : parent.width/16;
    implicitWidth: 0.7*parent.width
    implicitHeight: parent.width/5
    background: Rectangle{
        height: parent.height
        radius: parent.width/20
        color: Material.background
        border.color: parent.hovered ? Material.foreground : Material.primary
        border.width: parent.width/50
        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            y: parent.height/2-font.pointSize
            text: controls.myText
        }
    }
}
