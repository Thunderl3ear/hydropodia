import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Flickable{
    id: flickable
    y: parent.width/4
    height: parent.height-y
    width: parent.width
    contentHeight: dataColumn.height
    ColumnLayout{
        id: dataColumn
        width: 0.8*parent.width
        x: 0.1*parent.width
        Layout.fillWidth: true
        Repeater {
            model: ShopDataFruits{}
            delegate: FancyItem{}
        }
    }
    ScrollBar.vertical: ScrollBar { }
}

