import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Item{
    height: parent.height
    width: parent.width
    anchors.margins: 0

    GridLayout{

        width: parent.width
        columns: 2
        ScalingText{text: "Weather Controls"}
        Switch {
            Layout.alignment: Qt.AlignRight
            font.pointSize: (parent.width/24 < 1) ? 1 : parent.width/24;
            id: controls
            checked: true
        }
        ScalingText{text: "Humidity"}
        Slider {
            id: humiditySlider
            from: 0
            to: 100
            value: 65
            enabled: controls.checked
            ToolTip {
                parent: humiditySlider.handle
                visible: humiditySlider.hovered
                text: value+"%"
                y: parent.height

                readonly property int value: humiditySlider.valueAt(humiditySlider.position)

            }
        }
        ScalingText{text: "Daylight Interval"}
        RangeSlider {
            id: daytimeSlider
            from: 0
            to: 24
            first.value: 6
            second.value: 21
            enabled: controls.checked
            stepSize: 1
            snapMode: Slider.SnapAlways
            ToolTip {
                parent: daytimeSlider.handle
                visible: daytimeSlider.hovered
                text: value1+"-"+value2
                y: parent.height
                readonly property int value1: daytimeSlider.first.value
                readonly property int value2: daytimeSlider.second.value
            }
        }
        ScalingText{text: "Brightness"}
        Slider {
            id: brightnessSlider
            from: 0
            to: 100
            value: 45
            enabled: controls.checked
        }
        ScalingText{text: "Temperature"}
        SpinBox {
            id: box
            enabled: controls.checked
            from: 15
            to: 30
            value: 22
            editable: false
        }
    }
}
