import QtQuick

ListModel{
    ListElement{text: "Acacia"; icon: "qrc:/icons/icons8-acacia-80.png"}
    ListElement{text: "Maple"; icon: "qrc:/icons/icons8-maple-leaf-100.png"}
    ListElement{text: "Bonsai"; icon: "qrc:/icons/icons8-bonsai-100.png"}
    ListElement{text: "Birch"; icon: "qrc:/icons/icons8-birch-90.png"}
    ListElement{text: "Pine nuts"; icon: "qrc:/icons/icons8-trees-64.png"}
    ListElement{text: "Hazelnut"; icon: "qrc:/icons/icons8-nut-96.png"}
    ListElement{text: "Coconut"; icon: "qrc:/icons/icons8-coconut-96.png"}
    ListElement{text: "Coffee"; icon: "qrc:/icons/icons8-bean-128.png"}
}
