import QtQuick
import QtQuick.Controls
import QtQuick.Layouts



Item{
    height: parent.width/5
    width: parent.width
    z: 10
    property string text: "FancyLabel"
    Rectangle{
        id: bgColor
            height: parent.height
            width: parent.width
            radius: parent.width/20
            color: Material.background
            border.color: Material.primary
            border.width: parent.width/50
    }
    Label{
        font.pointSize: (parent.width/12 < 1) ? 1 : parent.width/12;
        y: parent.height/2-parent.width/12
        x: 3*bgColor.border.width
        width: parent.width-x
        height: parent.height
        text: parent.text
    }
}
