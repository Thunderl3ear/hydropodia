import QtQuick
import QtQuick.Controls
import QtQuick.Layouts



ComboBox{
    Layout.alignment: Qt.AlignRight
    textRole: "text"
    valueRole: "value"
    font.pointSize: (parent.width/24 < 1) ? 1 : parent.width/24;
}


