import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Item{
    id: myShop
    height: parent.height
    width: parent.width
    anchors.margins: 0

    SwipeView {
        id: shopview
        currentIndex: 0
        width: parent.width
        height: parent.height
        Pane {
            width: shopview.width
            height: shopview.height
            FancyLabel{text: "Fruits"}
            ScrollableListFruits{}
        }
        Pane {
            width: shopview.width
            height: shopview.height
            FancyLabel{text: "Berries"}
            ScrollableListBerries{}
        }
        Pane {
            width: shopview.width
            height: shopview.height
            FancyLabel{text: "Legumes"}
            ScrollableListLegumes{}
        }
        Pane {
            width: shopview.width
            height: shopview.height
            FancyLabel{text: "Trees"}
            ScrollableListTrees{}
        }
    }
    PageIndicator {
        count: shopview.count
        currentIndex: shopview.currentIndex
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        background: Rectangle{
            color: Material.background
            border.width: parent.width/25
            radius: height/2
            border.color: Material.foreground
        }
    }
}
