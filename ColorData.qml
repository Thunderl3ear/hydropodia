import QtQuick


/*
Material.Red
#F44336
Material.Pink
#E91E63 (default accent)
Material.Purple
#9C27B0
Material.DeepPurple
#673AB7
Material.Indigo
#3F51B5 (default primary)
Material.Blue
#2196F3
Material.LightBlue
#03A9F4
Material.Cyan
#00BCD4
Material.Teal
#009688
Material.Green
#4CAF50
Material.LightGreen
#8BC34A
Material.Lime
#CDDC39
Material.Yellow
#FFEB3B
Material.Amber
#FFC107
Material.Orange
#FF9800
Material.DeepOrange
#FF5722
Material.Brown
#795548
Material.Grey
#9E9E9E
Material.BlueGrey
*/
