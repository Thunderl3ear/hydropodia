import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Item{
    height: parent.height
    width: parent.width
    anchors.margins: 0
    FancyLabel{text: "Track new plant"}
    GridLayout{
        y: parent.width/4
        width: parent.width
        columns: 2
        ScalingText{text: "Choose plant"}
        ScalingComboBox{
            model: DataUser{}
        }
        ScalingText{text: "Select slot"}
        ComboBox{
            Layout.alignment: Qt.AlignRight
            model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
        }
        ScalingText{text: "Days to ripe"}
        SpinBox {
            from: 0
            value: 60
            editable: true
        }
        ScalingText{text: "Water [ml]"}
        SpinBox {
            from: 0
            to: 10000
            value: 120
            editable: true
        }
    }
    AcceptButton{
        myText: "Confirm";
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: parent.bottom
    }
}
