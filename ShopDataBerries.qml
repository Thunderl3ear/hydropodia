import QtQuick

ListModel{
    ListElement{text: "Cherry"; icon: "qrc:/icons/icons8-berry-128.png"}
    ListElement{text: "Raspberry"; icon: "qrc:/icons/icons8-raspberry-160.png"}
    ListElement{text: "Strawberry"; icon: "qrc:/icons/icons8-strawberry-100.png"}
}
