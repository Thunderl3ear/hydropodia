# A TouchScreen Hydropod Controller

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [About](#about)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `Qt Creator 6.2.3`.

## Setup
* Build and Run through QT Creator

## About
This project illustrates the usage of separating model/view in QML. It has been designed to look good on any small screen in portrait mode, landscape mode not supported currently. 

My main goal was to create a responsive, intuitive and visually appealing interface, using QML classes for easy extensions and modifications. All custom classes uses the four Material theme colours. In the settings tab the global colour scheme can be changed.

I could not find the time to also figure out how to add a QML unit test. Since it was covered so late in the course.

![main](./images/main.JPG)

![status](./images/status.JPG)

![weather](./images/weather.JPG)

![shop](./images/shop.JPG)

![theme](./images/theme.JPG)

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

