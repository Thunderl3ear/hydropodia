import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

ApplicationWindow{
    id: mainWindow
    width: 400
    height: 600
    visible: true
    title: qsTr("Hydropodia")

    header: ToolBar{
        height: 0.15*parent.width
        visible: (view.currentIndex !== 1) ? true : false
        RowLayout{
            Layout.preferredWidth: parent.width
            Layout.alignment: Qt.AlignCenter
            anchors.fill: parent
            spacing: 0
            Repeater {
                model: MenuDataNavigation{}
                delegate: FancyHeaderButton{}
            }
        }
    }

    SwipeView {
        id: view
        currentIndex: 1
        width: parent.width
        height: parent.height
        interactive: false
        spacing: 0
        Pane{
            Shop{}
        }
        Pane{
            FancyLabel{text: "Hydropodia"}
            ScrollableListMenu{}
        }
        Pane{
            StatusPage{}
        }
        Pane{
            PlantingPage{}
        }
        Pane{
            EnvironmentPage{}
        }
        Pane{
            SettingsPage{}
        }
    }
}
